FROM python:3.10-slim-buster as build

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
COPY pipe/ requirements.txt LICENSE.txt pipe.yml README.md /
RUN apt-get update && apt-get install --no-install-recommends -y \
      apt-transport-https=1.8.* \
      gnupg=2.* \
      curl=7.* \
      tar=1* \
      git=1:2.* && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --user --no-cache-dir -r /requirements.txt && \
    curl "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-494.0.0-linux-x86_64.tar.gz" -o "gcloudcli.tar.gz" && \
    tar -zxvf gcloudcli.tar.gz && \
    echo '2903ca208f9353ca4574d1b14e1e06d4638caad96d291116ed878c828d90ec4d gcloudcli.tar.gz' | sha256sum -c -


FROM python:3.10-slim-buster

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

WORKDIR /

# install gcloud cli
COPY --from=build /google-cloud-sdk google-cloud-sdk

RUN ./google-cloud-sdk/install.sh --quiet
ENV PATH=/google-cloud-sdk/bin:$PATH
RUN gcloud components install kubectl -q && \
    rm -rf /google-cloud-sdk/.install/.backup

# copy python env
COPY --from=build /root/.local /root/.local
# copy project files
COPY --from=build pipe.py /
COPY --from=build LICENSE.txt pipe.yml README.md /

ENV PATH=/root/.local/bin:$PATH

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python", "/pipe.py"]
