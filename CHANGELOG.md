# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 3.5.0

- minor: Update google-cloud-sdk to version 494.

## 3.4.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 3.4.0

- minor: Bump gcloud sdk to version 474.0.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update test template.

## 3.3.0

- minor: Bump gcloud sdk to version 463.0.0.

## 3.2.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 3.1.0

- minor: Bump google-cloud-cli version to 447.0.0.
- patch: Internal maintenance: bump pipes versions in pipelines config file, bump packages in requirements.
- patch: Internal maintenance: update Dockerfile, remove backup google-cloud sdk files.

## 3.0.0

- major: Update gcloud-sdk to 442.0, update gcloud kubectl component to 1.26+, add gcloud gke-gcloud-auth-plugin component.
- minor: Update bitbucket-pipes-toolkit and kubectl-run pipe to fix vulnerability with certify.
- patch: Internal maintenance: update pipes versions in pipelines config file.

## 2.2.0

- minor: Bump version of the cloud-sdk image to 404-slim and kubectl to 1.25.2.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update requirements.

## 2.1.0

- minor: Update docs: added support of KUBECTL_APPLY_ARGS
- patch: Internal maintenance: update bitbucket-pipes-toolkit to 3.2.0, kubectl-run to 3.1.2

## 2.0.0

- major: Bump google/cloud-sdk version to 347.0.0.
- patch: Update dependency kubectl to 1.19.9 version.

## 1.3.1

- patch: Internal maintenance: Update dependency kubectl to 1.19.3 version.
- patch: Internal maintenance: add bitbucket-pipe-release.

## 1.3.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.2.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.2.2

- patch: Update README: clear description for base64 encoded environment variables

## 1.2.1

- patch: Internal maintenance: Add gitignore secrets.

## 1.2.0

- minor: Bump google/cloud-sdk version to 286.0.0.

## 1.1.3

- patch: Automated test infrastructure setup

## 1.1.2

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 1.1.1

- patch: Internal maintenance: Add check for newer version.

## 1.1.0

- minor: The pipe now accepts directory path in RESOURCE_PATH variable

## 1.0.0

- major: Changes the SPEC_FILE varialbe to RESOURCE_PATH

## 0.1.4

- patch: Internal maintenance: Update pipe's base Docker image to google/cloud-sdk:273.0.0-slim.

## 0.1.3

- patch: Documentation updates

## 0.1.2

- patch: Fixed the pipe metadata

## 0.1.1

- patch: Internal maintenance: Update dependencies.

## 0.1.0

- minor: Initial release
